/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.up = function (knex) {
  return knex.schema.createTable("users", function (table) {
    table.increments("id").primary().notNullable();
    table.string("name").notNullable();
    table.string("username").notNullable();
    table.string("password").notNullable();
    table.string("phone").nullable();
    table.text("description").nullable();
    table.integer("role_id");
    table.timestamp("created_at");
  });
};

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.down = function (knex) {};
