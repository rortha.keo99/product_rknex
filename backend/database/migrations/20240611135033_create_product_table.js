/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.up = function (knex) {
  return knex.schema.createTable("products", function (table) {
    table.increments("id").primary().notNullable();
    table.string("title").notNullable();
    table.text("description").nullable();
    table.string("sku").notNullable().unique();
    table.string("brand").nullable();
    table.decimal("price", 10, 2).notNullable();
    table.integer("stock_quantity").notNullable().unsigned().defaultTo(0);
    table.integer("category_id");
    table.float("weight").nullable();
    table.string("dimension").nullable();
    table.string("image_url").nullable();
    table.boolean("is_active").defaultTo(true);
    table.timestamp("created_at");
  });
};

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.down = function (knex) {};
