const mysql = require("mysql2/promise");
const dbconn = mysql.createPool({
  host: "localhost",
  user: "root",
  password: "",
  database: "product_rknex",
  port: 3306,
  namedPlaceholders: true,
});

module.exports = {
  dbconn,
};
