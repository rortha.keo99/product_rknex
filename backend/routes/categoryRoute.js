const { getListCate, createCate } = require("../controllers/categoryController")

const Category = (app) => {
    app.get("/api/category", getListCate);
    app.post("/api/category", createCate);
}

module.exports = {
    Category,
}