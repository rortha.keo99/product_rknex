const { getListRole, createRole } = require("../controllers/roleController");

const Role = (app) => {
  app.get("/api/role", getListRole);
  app.post("/api/role", createRole);
};

module.exports = {
  Role,
};
