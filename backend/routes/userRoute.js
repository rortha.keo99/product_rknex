const { getListUser, createUser, updateUser, deleteUser } = require("../controllers/userController")

const User = (app) => {
    app.get("/api/user", getListUser);
    app.post("/api/user", createUser);
    app.put("/api/user", updateUser);
    app.delete("/api/user/:id", deleteUser);
}

module.exports = {
    User,
}