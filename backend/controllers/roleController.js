const { title } = require("process");
const { dbconn } = require("../database/server/connection");

const getListRole = async (req, res) => {
  const [dataList] = await dbconn.query("SELECT * FROM roles");
  const [dataCount] = await dbconn.query("SELECT COUNT(id) FROM roles");
  res.json({
    List: dataList,
    Count: dataCount,
  });
};

const createRole = async (req, res) => {
  try {
    const param = {
      title: req.body.title,
      description: req.body.description,
    };
    const sqlInsert =
      "INSERT INTO roles(title, description) VALUES(:title, :description)";
    const data = await dbconn.query(sqlInsert, param);
    res.json({
      message: "Data saved!",
      data: data,
    });
  } catch (error) {
    res.status(500).json({
      message: "Error saving data",
      error: error.message,
    });
  }
};

module.exports = {
  getListRole,
  createRole,
};
