const { dbconn } = require("../database/server/connection");
const getListCate = async (req, res) => {
    try {
        const [dataList] = await dbconn.query("SELECT * FROM categories");
        const [dataCount] = await dbconn.query("SELECT COUNT(id) FROM categories");
        res.json({
            list: dataList,
            count: dataCount
        });

    } catch (error) {
        res.status(500).json({
            message: "Data not retrive",
            error: message,
        });
    }
}

const createCate = async (req, res) => {
    const {title, description} = req.body;
    const param = [title, description];
    const sqlquery = "INSERT INTO categories(title, description) VALUES(?,?)";
    try {
        const [data] = await dbconn.query(sqlquery, param);
        res.json({
            message: "Data saved!",
            sql: data
        })
    } catch (error) {
        res.status(500).json({
            message: "Error saving data!",
            error: error.message
        });
    }
}

module.exports = {
    getListCate, createCate,
}