const { dbconn } = require("../database/server/connection")

const getListUser = async (req, res) => {
    const [dataList] = await dbconn.query("SELECT * FROM users");
    try {
        if (dataList.length > 0) {
            res.json({
                list: dataList
            })
        } else if (dataList.length == 0) {
            res.status(404).json({
                message: "No found data!"
            })
        } else {
            res.status(500).json({
                message: "Data undifine!"
            })
        }
    } catch (error) {
        res.status(500).json({
            message: "Error retrieving data",
            error: error.message
        })
    }
}

const createUser = async (req, res) => {
    const { name, username, password, phone, description, role_id } = req.body;
    const param = [name, username, password, phone, description, role_id];
    const paramCheck = [username, password];
    const sqlCheck = "SELECT COUNT(*) as counts FROM users WHERE username = ? AND password = ?";
    const sqlInsert = "INSERT INTO users(name, username, password, phone, description, role_id) VALUES(?,?,?,?,?,?)";
    try {
        const [checkdata] = await dbconn.query(sqlCheck, paramCheck);
        if (checkdata[0].counts > 0) {
            return res.status(400).json({
                message: "Username and password are taken by other user",
                sql: checkdata
            })
        } else {
            const [dataInsert] = await dbconn.query(sqlInsert, param);
            return res.json({
                message: "Data saved!",
                sql: dataInsert
            })
        }
    } catch (error) {
        return res.status(500).json({
            message: "Error inserting data",
            error: error.message
        })
    }
}

const updateUser = async (req, res) => {
    const { name, username, password, phone, description, role_id, id } = req.body;
    const param = [name, username, password, phone, description, role_id, id];
    const sqlUpdate = "UPDATE users SET name = ?, username = ?, password = ?, phone = ?, description = ?, role_id = ? WHERE id = ?";
    try {
        const data = await dbconn.query(sqlUpdate, param);
        if (data.affectedRows === 0) {
            res.status(404).json({
                message: "No matching users found",
            })
        } else {
            res.json({
                message: "Data updated!",
                sql: data
            })
        }
    } catch (error) {
        res.status(500).json({
            message: "Error updating data!",
            error: error.message
        })
    }
}

const deleteUser = async (req, res) => {
    const { id } = req.params;
    const param = [id];
    const sqlDelete = "DELETE FROM users WHERE id = ?";
    const data = await dbconn.query(sqlDelete, param);
    try {
        if (data.affectedRows === 0) {
            return res.status(404).json({
                message: "User not found or deleted!",
            })
        } else {
            res.json({
                message: "Data deleted!",
                sql: data
            })
        }
    } catch (error) {
        res.status(500).json({
            message: "Error deleting data",
            error: error.message
        })
    }
}

module.exports = {
    getListUser, createUser, updateUser, deleteUser
}