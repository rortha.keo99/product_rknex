const express = require("express");
const { Role } = require("./backend/routes/roleRoute");
const { Category } = require("./backend/routes/categoryRoute");
const { User } = require("./backend/routes/userRoute");
const app = express();
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

Role(app);
Category(app);
User(app);

const port = 8081;
app.listen(port, () => {
  console.log("http://localhost:" + port);
});
